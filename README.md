# Nginx Example for Keycloak

This is an example for an Nginx Config based on Debian for reverse proxy a Keycloak installation.

Nginx is running rootless, please beware of certificate permissions.

# Keycloak

To get this running, Keycloak entrypoint must contain an --proxy directive:

```
ENTRYPOINT ["/opt/keycloak/bin/kc.sh", "start", "--db postgres", "--db-url jdbc:postgresql://172.17.0.1:5432/keycloak", "--proxy reencrypt"]
```

Also hostname must be set in .env and must match DNS entry for keycloak:
```
KC_HOSTNAME=kc.example.com
```

Keycloak verifies hostname, so access will fail if called hostname does not match KC_HOSTNAME.

# References

* https://www.keycloak.org/server/reverseproxy

