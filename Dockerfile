from debian:buster-slim
maintainer Christian Felsing <support@felsing.net>

ARG hostname=kc
ARG domain=cf.felsing.net
ARG fqname=${hostname}.${domain}

run DEBIAN_FRONTEND=noninteractive apt-get update
run DEBIAN_FRONTEND=noninteractive apt-get install -q -y apt-utils
run DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
  locales

RUN sed -i -e 's/# de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG de_DE.UTF-8
ENV LANGUAGE de_DE:de
ENV LC_ALL de_DE.UTF-8

run DEBIAN_FRONTEND=noninteractive apt update && apt -q -y full-upgrade

run DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
  procps \
  cron \
  curl \
  nginx

RUN rm -rf /etc/nginx

ARG ENTRYP=/entrypoint.sh
ARG WORKDIR=/var/lib/nginx
COPY ./entrypoint.sh ${ENTRYP}
RUN mkdir -p ${WORKDIR} /run/nginx
RUN \
  chown -R www-data:www-data ${ENTRYP} ${WORKDIR} /run/nginx \
  && chmod 700 ${ENTRYP} \
  && chmod 755 ${WORKDIR}

USER www-data
ENTRYPOINT [ "/entrypoint.sh" ]

